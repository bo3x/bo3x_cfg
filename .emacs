(setq backup-by-copying 1) ; should be the first line
(setq backup-directory-alist '(("." . "~/.emacs_saves")))

(set-face-attribute
 'default nil
 :family "PT Mono"
 :foundry "paratype"
 :slant 'normal
 :weight 'normal
 :height 122
 :width 'normal)
(load-theme 'deeper-blue)
(global-linum-mode 1)
(column-number-mode 1)
(display-time-mode 1)
(show-paren-mode 1)
(tool-bar-mode -1)
(global-hl-line-mode 1)

(setq-default indent-tabs-mode nil)

(add-to-list 'load-path "~/.emacs.d/extra")
(add-to-list 'auto-mode-alist '("\\.pac\\'" . js-mode))
(add-to-list 'auto-mode-alist '("\\.lua\\'" . lua-mode))
(add-to-list 'auto-mode-alist '("\\.txt\\'" . text-mode))

;;*** MELPA ************************************************
(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))
(package-initialize)
;;**********************************************************

;;*** auto-complete ****************************************
(require 'auto-complete)
(require 'auto-complete-config)
(ac-config-default)

(defun bdsm:ac-c-header-init ()
  (require 'auto-complete-c-headers)
  (add-to-list 'ac-sources 'ac-source-c-headers))
;;**********************************************************

;;*** Picolisp *********************************************
(add-to-list 'load-path "~/opt/picolisp/lib/el")
(autoload 'run-picolisp "inferior-picolisp")
(autoload 'picolisp-mode "picolisp" "Major mode for editing Picolisp." t)
(add-to-list 'auto-mode-alist '("\\.l$" . picolisp-mode))
;;**********************************************************

(set-input-method "russian-computer")
;;*** spell checker ****************************************
(when (string-equal system-type "windows-nt")
   (add-to-list 'exec-path "C:/cygwin64/bin/"))
(setq ispell-program-name "aspell")

(defun bdsm:switch-dict ()
  (interactive)
  (let* ((dic ispell-current-dictionary)
	 (change (if (string= dic "english") "russian" "english")))
    (ispell-change-dictionary change)
    (message "Dictionary switched from %s to %s" dic change)))

(defun bdsm:flyspell-check-next-highlighted-word ()
  "Custom function to spell check next highlighted word"
  (interactive)
  (flyspell-goto-next-error)
  (ispell-word))

(global-set-key (kbd "<f8>") 'bdsm:switch-dict)
(global-set-key (kbd "C-M-<f8>") 'flyspell-mode)
(global-set-key (kbd "M-<f8>") 'flyspell-buffer)
(global-set-key (kbd "<f7>") 'ispell-word)
(global-set-key (kbd "M-p") 'flyspell-check-previous-highlighted-word)
(global-set-key (kbd "C-<f7>") 'bdsm:flyspell-check-next-highlighted-word)
(require 'ispell)
(require 'flyspell)
;;**********************************************************

;;*** hooks ************************************************
(add-hook 'c++-mode-hook
          (lambda()
            (bdsm:ac-c-header-init)
            (local-set-key (kbd "C-c m") 'ff-find-other-file) ))
(add-hook 'c-mode-hook
          (lambda()
            (bdsm:ac-c-header-init)
            (flycheck-mode)
            (outline-minor-mode)
            (local-set-key (kbd "C-c m") 'ff-find-other-file) ))
(add-hook 'lua-mode-hook 'flycheck-mode)
(add-hook 'text-mode-hook
	  (lambda()
	    (setq tab-width 4)
	    (setq indent-tabs-mode t)
	    (typo-mode)
	    (flyspell-mode 1)))
;;**********************************************************

(require 'sr-speedbar)
(require 'fvwm-mode)

;;(EM DASH: codepoint 8212, #o20024, #x2014)
(define-key 'iso-transl-ctl-x-8-map "2" [#x2014])

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   (quote
    (lua-mode websocket web-server typo markdown-mode fvwm-mode flymd flycheck auto-complete))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
